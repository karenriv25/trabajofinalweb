function validar1(){
    var express;
    var usua = document.getElementById("usuarioo").value;
    var contras = document.getElementById("contra").value;
    var correoe = document.getElementById("correo").value;
    var telef = document.getElementById("telefono").value;

    express=/\w+@\w+\.+[a-z]/; //para que el correo este bien 

    if(usua == "" || contras == "" || correoe == "" || telef == "" ){
        alert("Todo los campos son obligatorios");
        return false;
    }
    else if (usua.length>30){
        alert("Nombre de usuario muy largo");
        return false;
    }
    else if (contras.length>30){
        alert("Contraseña muy largo");
        return false;
    }
    else if (correoe.length>80){
        alert("Nombre de usuario muy largo");
        return false;
    }
    else if(!express.test(correoe)){ //evaluar si cumple con la expresin 
        alert("El correo no es valido");
        return false;
    }
    else if (telef.length>15){
        alert("El telefono es muy largo");
        return false;
    }
    else if (isNaN(telef)){
        alert("El telefono no es un número");
        return false;
    }
}
